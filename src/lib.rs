use std::io::{Read,BufReader};
use std::fs::File;
use std::error::Error;

pub fn read(path: &String) -> Result<String, Box<Error>> {
    let mut data = String::new();
    let f = try!(File::open(path));
    let mut rdr = BufReader::new(f);
    try!(rdr.read_to_string(&mut data));
    Ok(data)
}

pub fn shannons_entropy(data: &String) -> f64 {
    let mut counts: [f64; 256] = [0_f64; 256];
    for c in data.bytes() {
        counts[c as usize] += 1_f64;
    }
    let mut ent = 0_f64;
    let sum = data.len() as f64;
    for i in 0..256 {
        if counts[i as usize] == 0_f64 {
            continue;
        }
        let p: f64 = counts[i as usize] / sum;
        ent += p * p.log(2_f64);
    }
    -1_f64 * ent
}

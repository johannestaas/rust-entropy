extern crate docopt;
extern crate rustc_serialize;
use std::io::Write;
use docopt::Docopt;

mod lib;

static USAGE: &'static str = "
Usage: entropy [options] <filepath>
       entropy --help

Options:
    -h, --help      Show this error message
";

#[derive(Debug, RustcDecodable)]
struct Args {
    arg_filepath: String,
}


fn main() {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.decode())
        .unwrap_or_else(|err| err.exit());
    let val = match lib::read(&args.arg_filepath) {
        Ok(val) => val,
        Err(e) =>  { 
            writeln!(&mut std::io::stderr(), "Unable to open {}: {}", 
                &args.arg_filepath, e.description()); 
            std::process::exit(1); 
        },
    };
    let ent = lib::shannons_entropy(&val);
    println!("{}", ent);
}
